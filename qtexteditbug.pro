#-------------------------------------------------
#
# Project created by QtCreator 2014-08-02T18:04:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qtexteditbug
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h
