#include "widget.h"

Widget::Widget(QWidget *parent) : QWidget(parent)
{
    textEdit = new QTextEdit();
    textEdit->setAcceptRichText(true);

    QPushButton *button = new QPushButton("&Read HTML >");
    connect(button, SIGNAL(clicked()),
            this, SLOT(copyHtmlToLabel()));

    label = new QLabel("This label will show the real contents read"
                       "from the QTextEdit on the left.");
    label->setFrameStyle(QFrame::Box);
    label->setWordWrap(true);
    label->setAlignment(Qt::AlignTop);
    label->setTextFormat(Qt::PlainText);


    QHBoxLayout *layout = new QHBoxLayout();
    layout->addWidget(textEdit,  4);
    layout->addWidget(button,    1);
    layout->addWidget(label,     4);
    this->setLayout(layout);

    this->resize(800, 500);

    qDebug() << "Widget created";
}

Widget::~Widget()
{
    qDebug() << "Widget destroyed";
}


void Widget::copyHtmlToLabel()
{
    this->label->setText(this->textEdit->toHtml());
}
